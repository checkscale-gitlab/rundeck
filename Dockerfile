FROM rundeck/rundeck:3.2.1

ENV ANSIBLE_VERSION "2.9.4-1ppa~xenial"
#ENV BATIX_PLUGIN_VERSION "3.0.1"

USER root

RUN set -eux; \
    DEBIAN_FRONTEND=noninteractive; \
    apt-get update; \
    apt-get install --no-install-recommends --no-install-suggests -y gnupg; \
    \
    ANSIBLE_GPGKEY=6125E2A8C77F2818FB7BD15B93C4A3FD7BB9C367; \
    found=''; \
    for server in \
        ha.pool.sks-keyservers.net \
        hkp://keyserver.ubuntu.com:80 \
        hkp://p80.pool.sks-keyservers.net:80 \
        pgp.mit.edu \
    ; do \
        echo "Fetching GPG key $ANSIBLE_GPGKEY from $server"; \
        apt-key adv --keyserver "$server" --keyserver-options timeout=10 --recv-keys "$ANSIBLE_GPGKEY" && found=yes && break; \
    done; \
    \
    test -z "$found" && echo >&2 "error: failed to fetch GPG key $ANSIBLE_GPGKEY" && exit 1; \
    \
    apt-get remove --purge --auto-remove -y gnupg && rm -rf /var/lib/apt/lists/*; \
    echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu xenial main" >> /etc/apt/sources.list.d/ansible.list; \
    \
    apt-get update; \
    apt-get install --no-install-recommends --no-install-suggests -y ansible=${ANSIBLE_VERSION}; \
    rm -rf /var/lib/apt/lists/* /etc/apt/sources.list.d/ansible.list;

RUN set -eux; \
    DEBIAN_FRONTEND=noninteractive; \
    apt-get update; \
    apt-get install --no-install-recommends --no-install-suggests -y python-pip; \
    pip install jsondiff pyyaml; \
    rm -rf /var/lib/apt/lists/* /etc/apt/sources.list.d/ansible.list;

USER rundeck
#RUN set -eux; \
#    mkdir -p /home/rundeck/libext; \
#    curl -L -o /home/rundeck/libext/ansible-plugin-${BATIX_PLUGIN_VERSION}.jar https://github.com/Batix/rundeck-ansible-plugin/releases/download/${BATIX_PLUGIN_VERSION}/ansible-plugin-${BATIX_PLUGIN_VERSION}.jar;